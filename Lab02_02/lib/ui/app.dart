import 'package:flutter/material.dart';
import '../stream/vocabularies_stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>{
  String vocabulary = "Click start button to learn new vocabularies";
  VocabularyStream vocabularyStream = VocabularyStream();

  int count = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Vocabularies',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Stream Vocabularies"),
        ),
        body: Column(
          children: [
            Container(margin: EdgeInsets.symmetric(vertical: 50),),
            Text(vocabulary, style: const TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
            Container(margin: EdgeInsets.symmetric(vertical: 50),),
            Text(count.toString(), style: const TextStyle(fontSize: 20,),),
            Container(margin: EdgeInsets.only(top: 60.0),),

          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('start'),
          onPressed: () {
            changeString();
          },
        ),
      ),
    );
  }

  changeString() async{
    vocabularyStream.getVocabulary().listen((eventColor) {
      setState(() {
        vocabulary = eventColor;
        count++;
      });
    });
  }
}
