import 'package:flutter/material.dart';

class VocabularyStream{
  Stream<String> getVocabulary() async*{
    final List<String> vocabularies = [
      "Very Accurate – Precise, Exact, Unimpeachable, Perfect, Flawless",
      "Very Aggressive – Forceful, Overconfident, Insistent, Hardline",
      "Very Angry – Annoyed, Furious, Irate, Enraged, Incensed, Fuming, Livid",
      "Very Annoying – Irksome, Infuriating, Exasperating",
      "Very Awake – Rested",
      "Very Bad – Horrendous, Atrocious, Horrible, Awful",
      "Very Beautiful – Stunning, Astonishing, Charming, Magnificent",
      "Very Big – Giant, Immense, Massive, Enormous, Huge, Giant, Tremendous, Humongous",
      "Very Bright, Light – Luminous, Dazzling",
      "Very Busy – Swamped",
      "Very Calm – Serene",
      "Very Careful – Prudent, Cautious",
      "Very Careless – Reckless",
      "Very Charming – Enchanting",
      "Very Cheap – A bargain, Inexpensive, Low-Budget, Stingy",
      "Very Clean – Spotless, Tidy",
      "Very Clear – Obvious"
    ];

    yield* Stream.periodic(Duration(seconds: 2), (int t) { // seconds to change words
      int index = t - 1;
      return vocabularies[index];
    });
  }
}